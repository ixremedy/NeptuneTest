package com.forthreal.application

import com.forthreal.application.classes.db.NeptuneClusterInstance
import org.apache.tinkerpop.gremlin.driver.remote.DriverRemoteConnection
import org.apache.tinkerpop.gremlin.process.traversal.AnonymousTraversalSource.traversal
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversal.Symbols.id
import org.apache.tinkerpop.gremlin.structure.VertexProperty
import org.slf4j.LoggerFactory
import java.lang.invoke.MethodHandles
import java.util.Random

/* adding Vertices:
 * g.addV('InterestArea').property('name','europe').property('keyid','1')
 * g.addV('InterestArea').property('name','sales').property('keyid','2')
 * g.addV('InterestArea').property('name','therapeutic-pills').property('keyid','3')
 * g.addV('InterestArea').property('name','antibiotics').property('keyid','4')
 * adding Edges:
 * g.V().has('keyid','4').as('higher').V().has('keyid','3').addE('connected').to('higher')
 * g.V().has('keyid','3').as('higher').V().has('keyid','2').addE('connected').to('higher')
 * g.V().has('keyid','2').as('higher').V().has('keyid','1').addE('connected').to('higher')
 * traverse:
 * g.V().has('keyid','1').out().values()
 */

object MainApp
{
    val logger = LoggerFactory.getLogger( MethodHandles.lookup().lookupClass().simpleName )

    @JvmStatic
    fun main(args: Array<String>)
    {

        val writerCluster =
            NeptuneClusterInstance (
                "testdb-cluster.cluster-civvkbvrtpcp.eu-central-1.neptune.amazonaws.com",
                8182,
                "SFSRootCAG2.pem"
                    )
                .cluster

        val g = traversal().withRemote( DriverRemoteConnection.using(writerCluster) )

        var longVal : Long = 0
        while( longVal <= 0)
        {
            longVal = Random().nextLong()
        }

        /* AWS Neptune doesn't support numeric IDs for Vertices, so using of id with a number type will fail */
        g.addV("MyVertex")
            .property(VertexProperty.Cardinality.single, id,"longVal")
            .property(VertexProperty.Cardinality.single,"name", "a${longVal}" )
            .next()

        g.V().forEach {
            System.out.println("found ${it.label()}[${it.id()}]: ${it.keys().size} ")
        }

        /* iteration doesn't occur automatically after a drop */
        g.V().drop().iterate()

        writerCluster.close()
    }
}