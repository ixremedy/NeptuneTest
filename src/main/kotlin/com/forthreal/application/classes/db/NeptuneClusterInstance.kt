package com.forthreal.application.classes.db

import org.apache.tinkerpop.gremlin.driver.Cluster
import org.apache.tinkerpop.gremlin.driver.ser.Serializers

class NeptuneClusterInstance(val endpoint: String, val port: Int, val certFileName: String)
{
    val cluster: Cluster

    init {

        val builder = Cluster.build()

        builder
            .addContactPoint( endpoint )
            .port( port )
            .serializer( Serializers.GRAPHBINARY_V1D0 )
            .enableSsl( true )
            .keyCertChainFile( certFileName )

        cluster = builder.create()

    }
}